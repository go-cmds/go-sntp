sntp
====

### A implementation of NTP server with Golang
##### What this?
- Base on [RFC 2030](http://tools.ietf.org/html/rfc2030)
- Multiple equipments sync time from local
- Design for multiple equipments which can't connect to internet and need synchronization time
- Compatible with [ntpdate](http://www.eecis.udel.edu/~mills/ntp/html/ntpdate.html) service on the linux
- NTP client is forks from [beevik](https://github.com/beevik/ntp/), and [btfak](https://github.com/btfak/sntp)

#### Usage manual
##### 1. install Golang

https://go.dev/doc/install

##### 2. install Sntp
```
go get gitlab.com/go-cmds/go-sntp
```
##### License
[Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).
