//package main
//create: 2013-9-25
//update: 2022-11-22

package main

import (

    "flag"
    "os"
    "fmt"

    "gitlab.com/go-cmds/go-sntp/netapp"
    "gitlab.com/go-cmds/go-sntp/netevent"
)

func main() {

    var ver = "1.0.1"
    var version bool
    flag.BoolVar(&version, "version", false, "version output")
    flag.Parse()
    if version {
        fmt.Println("go-sntp version is " + ver)
        os.Exit(0)
    }


	var handler = netapp.GetHandler()
	netevent.Reactor.ListenUdp(123, handler)
	netevent.Reactor.Run()
}
